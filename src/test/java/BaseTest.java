import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;

import static utils.ScreenshotUtil.captureScreenshot;
import static utils.ScreenshotUtil.deleteOldScreenshots;

public class BaseTest {

    protected WebDriver driver;

    @BeforeClass
    public static void testClassSetUp() {
        // set paths to browser drivers using webdriver manager
        WebDriverManager.firefoxdriver().setup();
    }

    @Rule
    public TestRule testWatcher = new TestWatcher() {

        @Override
        public void starting(Description d) {
            deleteOldScreenshots(d.getMethodName());
        }

        @Override
        public void failed(Throwable e, Description d) {
            captureScreenshot(driver, d.getMethodName());
        }

        @Override
        protected void finished(Description desc) {
            driver.close();
        }
    };

}
