import org.junit.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import pageObjects.CartPage;
import pageObjects.ProductDetailPage;
import pageObjects.SearchResultPage;
import pageObjects.UpperMenu;

import static org.hamcrest.CoreMatchers.containsString;

public class AmazonOrderTest extends BaseTest {

    private UpperMenu upperMenu;
    private SearchResultPage searchResultPage;

    @Before
    public void testSetup() {
        driver = new FirefoxDriver();
        driver.get("https://www.amazon.com");
        //driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        upperMenu = new UpperMenu(driver);
        searchResultPage = upperMenu.search("Echo Dot");
    }

    @Test
    public void simpleOrderTest() {
// open first search result
        ProductDetailPage productDetailPage = searchResultPage.openSearchResult(1);

        // add to cart
        productDetailPage.addToCart();

        // verify that product has been added to cart
        Assert.assertEquals("Added to Cart", productDetailPage.getConfirmText());

        // open cart
        CartPage cartPage = upperMenu.openCart();

        // verify product is present in the cart
        Assert.assertThat(cartPage.getProductTitleText(1),containsString("Echo Dot"));
    }
}
