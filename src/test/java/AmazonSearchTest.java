import org.junit.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import pageObjects.SearchResultPage;
import pageObjects.UpperMenu;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.containsString;
import static utils.ScreenshotUtil.captureScreenshot;

public class AmazonSearchTest extends BaseTest {

    private UpperMenu upperMenu;

    @Before
    public void testSetup() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.get("https://www.amazon.com");
        upperMenu = new UpperMenu(driver);
    }

    @Test
    public void simpleSearchTest() {
        // search for a product
        SearchResultPage searchResultPage = upperMenu.search("Echo Dot");

        // check search results
        Assert.assertThat(searchResultPage.getSearchResultsCount()
                ,containsString("results for \"Echo Dot\""));
        Assert.assertThat(searchResultPage.getSearchResultText(1)
                ,containsString("Echo Dot"));
    }

    @Test
    public void brandAmazonTest() {
        // search for a product
        SearchResultPage searchResultPage = upperMenu.search("Echo Dot");

        // select Brand = Amazon
        searchResultPage.selectAmazonBrand();

        // verify first search item contains text "by Amazon"
        // TODO: padá - proč?
        Assert.assertThat(searchResultPage.getSearchResultText(1)
                ,containsString("by Amazon"));
    }

}
