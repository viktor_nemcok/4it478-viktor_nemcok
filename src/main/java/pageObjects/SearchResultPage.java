package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SearchResultPage {

    private WebDriver driver;

    @FindBy(id="s-result-count")
    private WebElement searchResultsCount;

    @FindBy(css=".s-result-item h2")
    private List<WebElement> searchResultsTitles;

    @FindBy(className = "s-item-container")
    private List<WebElement> searchResultItems;

    @FindBy(name = "s-ref-checkbox-Amazon")
    private WebElement brandAmazonCheckBox;


    public SearchResultPage(WebDriver driver) {

        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getSearchResultsCount() {
        return searchResultsCount.getText();
    }

    public String getSearchResultText(int number) {
        return searchResultsTitles.get(number - 1).getText();
    }

    public ProductDetailPage openSearchResult(int number) {
        searchResultsTitles.get(number-1).click();
        return new ProductDetailPage(driver);
    }

    public void selectAmazonBrand() {
        //TODO: use brand as parameter - will be shown later
        brandAmazonCheckBox.click();
        // wait until checkbox selected (it's the last part loaded at the page after selection)
        WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.attributeToBe(brandAmazonCheckBox, "value", "true"));
    }

    public void getSearchResultItemText(int number) {
        searchResultItems.get(number - 1).getText();
    }
}
