package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductDetailPage {

    private WebDriver driver;

    @FindBy(id="add-to-cart-button")
    private WebElement addToCartButton;

    @FindBy(css="h1")
    private WebElement confirmHeading;

    public ProductDetailPage(WebDriver driver) {

        this.driver = driver;

        PageFactory.initElements(driver, this);
    }

    public void addToCart() {
        addToCartButton.click();
    }

    public String getConfirmText() {
        return confirmHeading.getText();
    }

}
