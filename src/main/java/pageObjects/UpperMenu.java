package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UpperMenu {

    private WebDriver driver;

    @FindBy(id="searchDropdownBox")
    private WebElement searchSelect;

    @FindBy(id = "twotabsearchtextbox")
    private WebElement searchField;

    @FindBy(css = "form.nav-searchbar input[type='submit']")
    private WebElement searchBtn;

    @FindBy(className = "nav-cart-icon")
    private WebElement cartIcon;

    @FindBy(css="option[value='search-alias=electronics-intl-ship']")
    private WebElement electronicsCategory;

    public UpperMenu(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public SearchResultPage search(String searchTerm) {
        searchField.sendKeys(searchTerm);
        searchBtn.click();
        return new SearchResultPage(driver);
    }

    public CartPage openCart() {
        cartIcon.click();
        return new CartPage(driver);
    }

}
