package utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScreenshotUtil {

    //Target path for screenshots
    private static final String SCREENSHOTS_TARGET_PATH = "Reports/";

    /**
     * Takes screenshot of the currently displayed browser window
     *
     * @param driver webdriver to use for taking the screenshot
     * @return path to the created screenshot
     */
    public static void captureScreenshot(WebDriver driver, String testName) {
        TakesScreenshot ts = (TakesScreenshot) driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
        String filePath = SCREENSHOTS_TARGET_PATH + testName + "_" + timeStamp + ".png";
        File destination = new File(filePath);
        try {
            FileUtils.copyFile(source, destination);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Deletes old screenshots (.png files)
     */
    public static void deleteOldScreenshots(String testName) {
        File files[] = new File(SCREENSHOTS_TARGET_PATH).listFiles();
        for (File file : files) {
            if (file.getName().startsWith(testName) && file.getName().endsWith(".png")) {
                file.delete();
            }
        }
    }



}
